<?php

use Illuminate\Support\Facades\Route;
use App\Models\User;
use App\Models\Post;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/create', function() {
    $user = User::findOrFail(2);

    $post = new Post(['title'=>'New Title 2', 'body' =>'content title 2']);
    
    $user->posts()->save($post);

});

Route::get('/read', function() {
   
    $user = User::findOrFail(2);
    
    foreach($user->posts as $post) {

        echo $post->title. "<br>";

    }
      
});

Route::get('/update', function() {

    $user = User::findOrFail(1);
    
    $user->posts()->whereId(1)->update(['title'=>"Title Update", 'body'=>'New content update']);

});

Route::get('/delete', function() {

    $user = User::findOrFail(2);
    $user->posts()->whereId(4)->delete();
});
